const iconButton = document.querySelector("i");
iconButton.onclick = show;
const input = document.querySelector("input");
const icon = document.getElementById("i");

function show() {
    if (input.getAttribute('type') == 'password') {
        input.removeAttribute('type');
        input.setAttribute('type', 'text');
        icon.className = 'fas fa-eye-slash';
    } else {
        input.removeAttribute('type');
        input.setAttribute('type', 'password');
        icon.className = 'fas fa-eye';
    }
}

const validateButton = document.getElementById('btn1');
validateButton.addEventListener('click', valid);

function valid() {
    if (document.getElementById('input1').value !== document.getElementById('input2').value) {
        document.getElementById('error').innerText = 'Нужно ввести одинаковые значения';
        document.getElementById('error').style.color = 'red';
    } else {
        document.getElementById('error').innerText = '';
        alert('You are welcome');
    }
}

